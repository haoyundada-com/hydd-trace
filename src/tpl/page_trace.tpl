<div id="haoyundada_page_trace" style="position: fixed;bottom:0;right:0;font-size:14px;width:100%;z-index: 999999;color: #000;text-align:left;font-family:'微软雅黑';">
    <div id="haoyundada_page_trace_tab" style="display: none;background:white;margin:0;height: 250px;">
        <div id="haoyundada_page_trace_tab_tit" style="height:30px;padding: 6px 12px 0;border-bottom:1px solid #ececec;border-top:1px solid #ececec;font-size:16px">
            <?php foreach ($trace as $key => $value) {?>
            <span style="color:#000;padding-right:12px;height:30px;line-height:30px;display:inline-block;margin-right:3px;cursor:pointer;font-weight:700"><?php echo $key ?></span>
            <?php }?>
        </div>
        <div id="haoyundada_page_trace_tab_cont" style="overflow:auto;height:212px;padding:0;line-height: 24px">
            <?php foreach ($trace as $info) {?>
            <div style="display:none;">
                <ol style="padding: 0; margin:0">
                    <?php
                    if (is_array($info)) {
                        foreach ($info as $k => $val) {
                            echo '<li style="border-bottom:1px solid #EEE;font-size:14px;padding:0 12px">' . (is_numeric($k) ? '' : $k.' : ') . htmlentities(print_r($val,true), ENT_COMPAT, 'utf-8') . '</li>';
                        }
                    }
                    ?>
                </ol>
            </div>
            <?php }?>
        </div>
    </div>
    <div id="haoyundada_page_trace_close" style="display:none;text-align:right;height:15px;position:absolute;top:10px;right:12px;cursor:pointer;"><img style="vertical-align:top;" src="data:image/gif;base64,R0lGODlhDwAPAJEAAAAAAAMDA////wAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUQxMjc1MUJCQUJDMTFFMTk0OUVGRjc3QzU4RURFNkEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUQxMjc1MUNCQUJDMTFFMTk0OUVGRjc3QzU4RURFNkEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxRDEyNzUxOUJBQkMxMUUxOTQ5RUZGNzdDNThFREU2QSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxRDEyNzUxQUJBQkMxMUUxOTQ5RUZGNzdDNThFREU2QSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAAAAAAALAAAAAAPAA8AAAIdjI6JZqotoJPR1fnsgRR3C2jZl3Ai9aWZZooV+RQAOw==" /></div>
</div>
<div id="haoyundada_page_trace_open" style="height:30px;float:right;text-align:right;overflow:hidden;position:fixed;bottom:0;right:0;color:#000;line-height:30px;cursor:pointer;">
    <div style="background:#232323;color:#FFF;padding:0 6px;float:right;line-height:30px;font-size:14px"><?php echo number_format(microtime(true)-\haoyundada\facade\App::getBeginTime(), 6, '.', '').'s ';?></div>
    <img width="30" style="" title="ShowPageTrace" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDYuMC1jMDA2IDc5LmRhYmFjYmIsIDIwMjEvMDQvMTQtMDA6Mzk6NDQgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NTRiZTBjNDctZmI3Ny00YTc1LTkyNDQtYWJmNjhhNTQ1ZTI3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjBBNzREMDdBNTY0MzExRUY4QTdEQTZFMzkwRUJFODYzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjBBNzREMDc5NTY0MzExRUY4QTdEQTZFMzkwRUJFODYzIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCAyMi40IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTRiZTBjNDctZmI3Ny00YTc1LTkyNDQtYWJmNjhhNTQ1ZTI3IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjU0YmUwYzQ3LWZiNzctNGE3NS05MjQ0LWFiZjY4YTU0NWUyNyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pvum1koAAA2USURBVHja7J0JbJzFFYBn/mNP79peX7EdE0hCIAWSQIDQcCvhLEVRo5amRC0FhASlFy0tLaXlUNSWHgLUVq2qtgRRVQWlQCEFIaAQIOLGgDlDbtuJz73P/5i+Z+82xvHx7+4/v3eWfdLLZNd7zOz3vzdvZt7MTxljpCbiiFT7CWrAalIDVpMasBqwmjghymwvoJRW9Q8wcnNXFxSngC4GPQq0EzQI6sfmg+qgKdBR0H157QF9vWnT/rCddbESsdPZXlSNwADSUiguBl0D2lrGR+0AfRL0PwBvoAbMXkjo/teCfh30aJs/Hn/EF0D/CuDeqQErH9aZUNwA2uXA170M+hsAt6sGrHhQ6O5uAj3L4a82QP8O+kcAl6sqYC93r6P54KcQsZrY4FUrHjbLhLUaijtA6+fwmvkILxiAtk9YYACoAYrTQVeCHgfaNs3wIgL6MeiboC8CwF1FwLoGimsqxNBTeWjbhQIGoJZBsQH0NFC5hI9AeFswKgN42gyBxQ9B11eYd8ahwR0AbWvFAwNQR0Dx3bxF2SEYPv8BoP13Eiys6C2gl1Zwt/qzmaDNKbB83/QV0KusDNBLkBdBfw7gYnlg10NxRYXHQWhp3wNoL1YUMIDlgeI20M9y/gEG0QUufvD1Y6C8XZDgFfu0jVMFInMCDGDhlM5vQZc60XqqmWbncx8FPOGkSCuxGD1eMTnktwLM1snfvGX90ilYKGo6u3Rk+fzFuaDHLRCwJaDXlvJGu2frbwRd5lSr5YzWTA3mMxVJGlo+v9NUZVkgaJdDv7tozoCBdV0ExXlOtRZ8uaxktI7CY9OtqsMndM4TCJiUv8CdBwawcDbheidbC9bVQtgnx3PZkL8u2R4MCATtZLCy5XNhYTgD7twPxZgkZ/WWqf4UXdzawiShJkCvchQYWFcTFJc4al05owGsa8qxnQGuMdHZEBQI2Gqwsg4nLexzoC5ngemhmf6e7GxsIGLJRU4Cu9DJllGTKVQ3Z3S/Wp3bowWECvPPdwQYuEOMyuY7GlpphqW+Mt1S5xcI2CJwi01OWNjxjsfCujVgmUa/TzC3eLITwLqcbhW4Q0sgNL/LLRiw45wA1uE4MJNZAmG6FIWIld7Q5QQwp8NnWkydGSUiEVvgBDCPo7RgwEyqVxqdAKY52iRGqjkN2ecEsAypiW3zAU4AG6rU1kNwwlAFAmY6Aazf4ZDDMgA5ncsJZmEpJ4B96GwXZh2YO5ZOCwYsYuVFig3AMPBQbfMLTAJPJkEpU4YJJYzhMzCkYvCMYeZdx6wXmns0lRIMWC93YKtWPJx9uXsdpmydU8r7R7JN7r2po+r6Uh3+qNbgiWqNbg1GvDN1TK3SoCskD0ltyoDZ5dpndLh6zcmekhqG6R2MJwUDtscJC0N5vBhgA5l2b0/k+MZdyUWNST1Q3LIM2NqI3kzDeqO8M7tEJkmiqkRjR7h2G5/x9RiLXDsNiRrEO5SMS4ZpCgbsQ6eAvQSKee8LZ3JzPdETGt4Ir2yL5Jq8ZfVjFIhMWLzUiEp35pYoqG6aYSd439Iv3L8lKmBY/5rFyYPy8xLBLeL+q01TjXPfDq8IvRpeNS+hBWyajGVUZblpoVONGRghnpLYHr4w8tBw0IgaIvRfTZv2r3M0kRSg3UnGNzqMx/vpTu8zA2u7hrOttq9LKUxzU2LKU4SRjKbMLHR144EHSxsXhB8ZPDP2VEQiFe0h/wbAfu90IikCi2CE9+zgue3/3LfhGB6wxlwslaa2mizTCrDGHlKv/O/Ql9vvbv/JglGlWalgYFstD0XtTNW+d9vN570ZPunewcw8zrP4jOTd4qHK5ZguZU192jAfrO2Lw5v7T0y+kqgwWG+BdY1lTjnqEtfct+NYKO7yyOmFQSXezruVEtNVmRjj4z+dGTRjatTCsHpNdOvgxeEtIxUE7FuFzX6OAQNYeM7Fr8n42RbEI2cCASXWTgnP2fWx4MMDQYZJstZgFeTU+LbRL41sHphtpouhfzeIPFYeagujOJKHq2WsLE8+AFgb//99FoCV7dcBFm7Uuwu9TuG5jOGJm4waATXeLhOTS98BEaiZ1TxhbyZZdO7GK4GzQiaV6YbhvxycDIglWNBIGXVmxvRNziw+7GKGQR9105Tkl2KyX45DRFDsZPOdxda9LAsDWLgL488FyzrMbVFTDijxVreUtbVP05mcTeiBAc1UM/NyfQsVppeUF3l27ImhS0cfGGYaU42I0WQkjPqSAzEMW+vksNwoj8K1oFt4xyNgXXd84oLh6RIBFiZr4tEGbbN9iUvKef1KslmlWlmZTAaTtLThG00b3ihY2FjF3WbG26IPLICGlOR+L9v9p8Tyvdv8ti2OAjilXhmSG+TwDKsLuMqBm/pijgADWPjk3aCri2mLKmkeCEqCADBg1VWa4Pp0U01lTE8sa7gTBVATJWhEQvV6pK04n8qorGkul56h1719a7YpPWDr2hlVaVptU/ugnLwqj9Z3FcB69/Aq8QP2NSi+WU6DZGoo4Dlc4DahHBsE0wldiQmqG0zWUKeCNFma9KF2n5G0lqINHy4BrMLDecn97Np3bs/Kpk5sFQph7Dx1v+SRJi713AawHp36GuIADGBhpu8DxOF8eivSoh3s8piZumJgFWTtvn/p5/Q+qnOolgmWtl/ySbjccxfAun96o+cz0/GDSoSFMqy29WYkb3wGNzglLJTnOi9Rwu5mHsMQSRvUuoyY8eBMsCx/WJEh/BnF9ltOCrrOIYCWkv2RqcZt0GdNu9CqyS7yxJGXKVyqpZGDWr92et/6lqCjwECuJgLIiNJyIKKEDjB6qO+TNF1ls0SC7zWtlAd9HdROVDBk2MsMhss9mCV9k2PAwLpOJXOw+aFUicvByIDavjsnudLEhKDGnD0JFXk+O//z9liZSaIAazeUEwOO88HKVjllYRuIYKJRV3ZQbd9rGtQgFhN4eppOkROuelqGVWXHrEpj/fD/qVYVvg3QKFdgYF2hSu67ZhKflggiKp2qWRN6sdnAwWtId/NpcgmgMgCpl+XYLrCqmRKAcHboDN4WdgEp7QS2ORd/LtZ4yEtJBoIzqJJjRJp2Jbq75XTJIiQdbCgMoPYAKHR/cYvVWscb2NkiwlJMTVXNw9MJcOLYAGsDeBmEByD1PMAx6zvo75KirhCd9CYTrQj+jQGmAeyfANIOprODk/opK3JGqRHjrB0suEMctywTEZhXT9ZZCDRMCPjzyySHnEhP/cr+1b1PxgghPNK98Ytwx+UzPCxseaUOlGcTl5EtebJ5d8Oxfk6wCnIiL5e4lAgq4BJLvtAO1B3Je+/bQl7AjhQVmGzqJQMLe5tdJuW6f7CTF7AFogKjpPQdm7girclunsSCvIC1kU+pZGUvT2AqL2B1n1ZguqTw3KKr8QLmF/UHx5C9LGCUK7AYL2DCbgQ3JKWsXZiM7zkf/byAGaIC0yW1LGAS31vw7uQFLCkqsJzsLmsXpsy4nmrRzQtYXFRgacVf1sWmmDovC0Ov9SovYAdEdokaLmCWKG49w2uP0vbOLUPcgo49IofmKbUuUsr7VCNruo00L2APldyvWnjNXpGBJV3BqDG+cFmUhNKDvM75wLs0Pc8T2FsiA8NMqri7segTe7piu3id83EPuEPGE9gHIkeKY1amBqKaVFzEeFTkfR5tfhpgbS/nA2YF9vRXj0Y//hoRXEa8rX3TbrWdPFPATHLMSLfdB7Pgfc82lfshVic3nxYdmCEpetjT0kcsLEoeEf046dfidk4YIPwbS40MSwGGd8NLiw4to/iSAK1/NmgnHXzeznM+sqDfB1jv2fFhloCBW0RYT5EqEAjzY6Pe1t7pJoYxnF82+JJdkwW4Af4bAOsVu+pfzHrP/aRKJK34E0P+jj1TTQ6vPLAt7NFTdoy/0KI2AqxuO+tuGRhYGU5WPlct0DTJlR3wde5OuILDBRcpGzo7e99joza4wN+BXgmweu2ud7F55LifGe86XhVn7zIqmVF301BSDUYDuUjorP5H9YbMcKl7xLDbeBh0M4Aa5lXnooCBlX2w5r4dOK3yBVJFgnOOEIy8pxoa7izFG62eQ8bT0GY7yAz7ujfyQdkzAIr7GY2l7MDE5BG8AXYjqS65BS7IxwsP+ta3YHdRn1ffhItby4fpOEcZB0i2zTfy3ON8LhS/qiJYz+I4CYDN6aHO3A4Hg4ahC/hHlcDCcdntcw2LR1g/We4B7REcFob1PwJYMVEqXDIwaCT68huIxcOFK1Cw7/kptONdkSpdVqIkNBbHLNeR8VvMiyY/h/oLN3tTdmYrNBr7gKsFsjS0rFuh3g+J6BZsSUXOQ7sS9J0Kby+G49+B+j4maqdrW+543j1eU8HR40egG6Ge24nAYusRshPGaThT8GPQUAW0ERuIRy3dDbAq+n4sjh4hOwU0nBG5FnQ9sfcw6GLkfdBfiBIJzimwCeCW5IMSnB1xatJ4D+hm0K35FAdSA1Y8ONwiejnoWsJnRww25E3QB7FLFQlURQKbAA7PBsZjJM4DxfOCyzkwC6HgbMsLoI8DJGGzlCsW2CR42LctBl1BxvdSo3bkIeLSRmGXYiIfkmN+4d68Yv/UDZBSpErEFmA1qdJxWE1qwGpSA1YDVpMasJrUgFWR/E+AAQD57cpVdVgY3AAAAABJRU5ErkJggg==">
</div>

<script type="text/javascript">
    (function(){
        var tab_tit  = document.getElementById('haoyundada_page_trace_tab_tit').getElementsByTagName('span');
        var tab_cont = document.getElementById('haoyundada_page_trace_tab_cont').getElementsByTagName('div');
        var open     = document.getElementById('haoyundada_page_trace_open');
        var close    = document.getElementById('haoyundada_page_trace_close').children[0];
        var trace    = document.getElementById('haoyundada_page_trace_tab');
        var cookie   = document.cookie.match(/haoyundada_show_page_trace=(\d\|\d)/);
        var history  = (cookie && typeof cookie[1] != 'undefined' && cookie[1].split('|')) || [0,0];
        open.onclick = function(){
            trace.style.display = 'block';
            this.style.display = 'none';
            close.parentNode.style.display = 'block';
            history[0] = 1;
            document.cookie = 'haoyundada_show_page_trace='+history.join('|')
        }
        close.onclick = function(){
            trace.style.display = 'none';
            this.parentNode.style.display = 'none';
            open.style.display = 'block';
            history[0] = 0;
            document.cookie = 'haoyundada_show_page_trace='+history.join('|')
        }
        for(var i = 0; i < tab_tit.length; i++){
            tab_tit[i].onclick = (function(i){
                return function(){
                    for(var j = 0; j < tab_cont.length; j++){
                        tab_cont[j].style.display = 'none';
                        tab_tit[j].style.color = '#999';
                    }
                    tab_cont[i].style.display = 'block';
                    tab_tit[i].style.color = '#000';
                    history[1] = i;
                    document.cookie = 'haoyundada_show_page_trace='+history.join('|')
                }
            })(i)
        }
        parseInt(history[0]) && open.click();
        tab_tit[history[1]].click();
    })();
</script>
